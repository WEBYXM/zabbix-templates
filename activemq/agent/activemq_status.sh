#/bin/sh
QUEUENAME=$1
MQ_COMMAND=$2
#MQ_IP=(172.16.8.120 172.16.8.118 172.16.8.119)
MQ_IP=(192.168.101.106)
for g in ${MQ_IP[@]}
	do
	curl -uadmin:admin http://${g}:8161/admin/queues.jsp >/dev/null 2>&1
	if [ $? -eq 0 ]; then
	i=$g
case $MQ_COMMAND in
	Enqueued)
	curl -uadmin:admin http://${i}:8161/api/jolokia/read/org.apache.activemq:type=Broker,brokerName=localhost,destinationType=Queue,destinationName=${QUEUENAME}/EnqueueCount 2>/dev/null |/usr/bin/jq .value
	;;
	Dequeued)
	curl -uadmin:admin http://${i}:8161/api/jolokia/read/org.apache.activemq:type=Broker,brokerName=localhost,destinationType=Queue,destinationName=${QUEUENAME}/DequeueCount 2>/dev/null |/usr/bin/jq .value
	;;
	Pending)
	curl -uadmin:admin http://${i}:8161/api/jolokia/read/org.apache.activemq:type=Broker,brokerName=localhost,destinationType=Queue,destinationName=${QUEUENAME}/QueueSize 2>/dev/null |/usr/bin/jq .value
	;;
	Consumers)
	curl -u admin:admin http://${i}:8161/api/jolokia/read/org.apache.activemq:type=Broker,brokerName=localhost,destinationType=Queue,destinationName=${QUEUENAME}/ConsumerCount 2>/dev/null |/usr/bin/jq .value
	;;
	ServerStatus)
	curl -u admin:admin http://${i}:8161/api/jolokia/read/org.apache.activemq:type=Broker,brokerName=localhost,service=Health/CurrentStatus 2>/dev/null |jq .value|sed "s/^/${i}/"
	;;
	MemoryPercentUsage)
	curl -u admin:admin http://${i}:8161/api/jolokia/read/org.apache.activemq:type=Broker,brokerName=localhost/MemoryPercentUsage 2>/dev/null|jq .value
	;;
	StorePercentUsage)
	curl -u admin:admin http://${i}:8161/api/jolokia/read/org.apache.activemq:type=Broker,brokerName=localhost/StorePercentUsage 2>/dev/null |jq .value
	;;
	TempPercentUsage)
	curl -u admin:admin http://${i}:8161/api/jolokia/read/org.apache.activemq:type=Broker,brokerName=localhost/TempPercentUsage 2>/dev/null|jq .value
	;;
esac
	fi
done
